package org.komica.android.KomicaROM;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.MimeTypeMap;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortableField;
import com.google.android.gms.tasks.*;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.remoteconfig.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

//import java.net.URISyntaxException;
//import org.jsoup.nodes.Document;
//import org.jsoup.select.Elements;
//import android.text.Html;
//import android.view.MotionEvent;
//import android.view.View.OnTouchListener;

//@SuppressWarnings(value = { "all" })
//@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class KomicaROMActivity extends Activity {
    private static ProgressDialog pd;
	final static String TAG ="KomicaROMActivity";
    private final static String strUPDATELIST= "更新列表" ;
	private final static  String AUTHORITY = "org.komica.android.KomicaROM.KomicaROMActivity";
	private ValueCallback<Uri> mUploadMessage;
	private final static int FILECHOOSER_RESULTCODE = 0;
	
	private static final boolean experimentalFeature =false;
	private static float mDeceleration;
	private List<String> boardList;
	private List<String> favBoardList;
	private List<String> srchBoardList;
	private ListView listView1;
	private ListView listView2;
	private ListView listView3;
	private WebView webView;
	private final static int MSG_RESIZE_LAYOUT =1001;
	private final static int MSG_SEARCH =1002;
	private final static int MSG_LOAD_URL =1003;

    private final static int REQUEST_EXPORT_AGAIN =2001;
    private final static int REQUEST_IMPORT_AGAIN =2002;

    private String mEmail="";
    // --Commented out by Inspection (11/11/14 1:53 AM):String mToken="";
//    final static int MSG_UPDATE_BBS=1004;
//    final static int MSG_UPDATE_FINISH =1005;
//    final static int MSG_UPDATE_ERROR =1006;

	private static final String NGLIST_URL = "https://docs.google.com/spreadsheet/pub?key=0AswsP_GylljodFdvaEYyaWxPTkl5Nl9valozdDdsVnc&single=true&gid=5&range=A1%3AA10&output=csv";
	private static String LIVE_URL="http://2cat.or.tl/~tedc21thc/live";
	private static String K1_URL ="http://web.komica.org/bbsmenu.html";
	private static String K2_URL ="http://pink.komica.org/bbsmenu.htm";
	private FirebaseRemoteConfig mFirebaseRemoteConfig;

	private int lockCount=0;
	private static List<String> ngList =null;


	public void fetchConfig() {
      //  Log.d("fetchConfig",K1_URL);
        long cacheExpiration = 3600; // 1 hour in seconds
		// If developer mode is enabled reduce cacheExpiration to 0 so that
		// each fetch goes to the server. This should not be used in release
		// builds.
		if (mFirebaseRemoteConfig.getInfo().getConfigSettings()
				.isDeveloperModeEnabled()) {
			cacheExpiration = 0;
		}
		mFirebaseRemoteConfig.fetch(cacheExpiration)
				.addOnSuccessListener(new OnSuccessListener<Void>() {
					@Override
					public void onSuccess(Void aVoid) {
						// Make the fetched config available via
						// FirebaseRemoteConfig get<type> calls.
						mFirebaseRemoteConfig.activateFetched();
                        applyURLSetting();
					}
				})
				.addOnFailureListener(new OnFailureListener() {
					@Override
					public void onFailure( Exception e) {
						// There has been an error fetching the config
						Log.w(TAG, "Error fetching config: " +
								e.getMessage());
                        applyURLSetting();
					}
				});
	}
    private void applyURLSetting() {
		LIVE_URL = mFirebaseRemoteConfig.getString("liveurl");
		K1_URL = mFirebaseRemoteConfig.getString("k1url");
		K2_URL = mFirebaseRemoteConfig.getString("k2url");
		//Log.d("URL",K1_URL);

	}

	
	public static final class BoardlListTable implements BaseColumns {
		private BoardlListTable(){}
		
		public static final String TABLE_NAME = "BOARDLIST";
		public static final Uri CONTENT_URI 
			=  Uri.parse("content://" + AUTHORITY + "/"+TABLE_NAME);
		public static final Uri CONTENT_ID_URI_BASE
         	= Uri.parse("content://" + AUTHORITY + "/"+TABLE_NAME+"/");

		public static final String CONTENT_TYPE
        	= "vnd.android.cursor.dir/vnd.komica.KomicaROM";
		public static final String CONTENT_ITEM_TYPE
        	= "vnd.android.cursor.item/vnd.komica.KomicaROM";
		
		public static final String DEFAULT_SORT_ORDER = "_ID ASC";
		
		public static final String BOARD_URL = "BOARDURL";
		public static final String BOARD_NAME = "BOARDNAME";
		public static final String FAV = "FAV";
	}

	private void enableHttpResponseCache() {
	    try {
	        long httpCacheSize = 10 * 1024; // 10KB
	        File httpCacheDir = new File(getCacheDir(), "http");
	        Class.forName("android.net.http.HttpResponseCache")
	            .getMethod("install", File.class, long.class)
	            .invoke(null, httpCacheDir, httpCacheSize);
	    } catch (Exception httpResponseCacheNotAvailable) {
	    }
	}

	private void readNGLIST(){
		ngList = new ArrayList<>();
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(NGLIST_URL).openStream()));
			String s ;
			while((s=reader.readLine())!=null){
				if (s.length()==8){
					ngList.add(s);
				}
			}
			reader.close();
			//conn.disconnect();
		}catch(Exception e){
			//
		}
//Log.d("NGLIST",ngList.toString());
	}
	
	static final class DatabaseHelper extends SQLiteOpenHelper {
		private static final String DATABASE_NAME = "KomiceROM.db";
		private static final int DATABASE_VERSION = 2;
		final Context context;
		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);	
			this.context = context;
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.beginTransaction();
			String sql;
			sql = "CREATE  TABLE "+BoardlListTable.TABLE_NAME +
				" ( "+BoardlListTable._ID +" INTEGER PRIMARY KEY NOT NULL  UNIQUE , " +
				BoardlListTable.BOARD_NAME	+" TEXT UNIQUE ON CONFLICT REPLACE, "+
				BoardlListTable.BOARD_URL +" TEXT UNIQUE ON CONFLICT REPLACE, "+
				BoardlListTable.FAV +" BOOL DEFAULT FALSE) ";
			try{
				db.execSQL(sql);
				db.setTransactionSuccessful();
			} catch (SQLException e) { Log.e("ERROR", e.toString()); }
			db.endTransaction();
//			insertSample(db);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			String sql;
			if (oldVersion == 1){
				sql = "ALTER TABLE "+BoardlListTable.TABLE_NAME  +" RENAME TO "+ BoardlListTable.TABLE_NAME+"_OLD";
				db.execSQL(sql);
				sql = "CREATE  TABLE "+BoardlListTable.TABLE_NAME +
						" ( "+BoardlListTable._ID +" INTEGER PRIMARY KEY NOT NULL UNIQUE , " +
						BoardlListTable.BOARD_NAME	+" TEXT UNIQUE ON CONFLICT REPLACE, "+
						BoardlListTable.BOARD_URL +" TEXT UNIQUE ON CONFLICT REPLACE, "+
						BoardlListTable.FAV +" BOOL DEFAULT FALSE) ";
				db.execSQL(sql);
				sql = "INSERT INTO  "+BoardlListTable.TABLE_NAME+
						" ( "+BoardlListTable.BOARD_NAME	+" , "+
						BoardlListTable.BOARD_URL +") "+
						"SELECT " +BoardlListTable.BOARD_NAME	+" , "+
						BoardlListTable.BOARD_URL +" FROM " +
						BoardlListTable.TABLE_NAME+"_OLD";
				db.execSQL(sql);
				sql = "DROP TABLE "+BoardlListTable.TABLE_NAME+"_OLD";
				db.execSQL(sql);
				//db.execSQL("VACUUM");
			}
		}
	}

	public static class BoardProvider extends ContentProvider {
		 final UriMatcher uriMatcher;
		 final HashMap<String, String> projectionMap;
		 private static final int TUPLES =1;
		 private static final int TUPLE =2;
		 private DatabaseHelper databaseHelper;

		 public BoardProvider(){
			 // Create and initialize URI matcher.
	         uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	         uriMatcher.addURI(AUTHORITY, BoardlListTable.TABLE_NAME, TUPLES);
	         uriMatcher.addURI(AUTHORITY, BoardlListTable.TABLE_NAME + "/#", TUPLE);
	
	         // Create and initialize projection map for all columns.  This is
	         // simply an identity mapping.
	         projectionMap = new HashMap<>();
	         projectionMap.put(BoardlListTable._ID, BoardlListTable._ID);
	         projectionMap.put(BoardlListTable.BOARD_NAME, BoardlListTable.BOARD_NAME);
	         projectionMap.put(BoardlListTable.BOARD_URL, BoardlListTable.BOARD_URL);
	         projectionMap.put(BoardlListTable.FAV, BoardlListTable.FAV);
		 }
		
		 public static String[] appendSelectionArgs(String[] originalValues, String[] newValues) {
	         if (originalValues == null || originalValues.length == 0) {
	             return newValues;
	         }
	         String[] result = new String[originalValues.length + newValues.length ];
	         System.arraycopy(originalValues, 0, result, 0, originalValues.length);
	         System.arraycopy(newValues, 0, result, originalValues.length, newValues.length);
	         return result;
		 }
		 
		 public static String concatenateWhere(String a, String b) {
	         if (TextUtils.isEmpty(a)) {
	             return b;
	         }
	         if (TextUtils.isEmpty(b)) {
	             return a;
	         }
	         return "(" + a + ") AND (" + b + ")";
	     }

		@Override
		public int delete(Uri uri, String selection, String[] selectionArgs) {
			int count;
			SQLiteDatabase db;
			switch (uriMatcher.match(uri)) {
			case TUPLES :
				db = databaseHelper.getWritableDatabase();
				count = db.delete(BoardlListTable.TABLE_NAME, selection, selectionArgs);
				db.close();
				break;
			case TUPLE :
				db = databaseHelper.getWritableDatabase();
				selection = concatenateWhere(BoardlListTable._ID + "="+ ContentUris.parseId(uri) ,selection);
				count = db.delete(BoardlListTable.TABLE_NAME, selection, selectionArgs);
				db.close();
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}

			getContext().getContentResolver().notifyChange(uri, null);
			return count;
		}

		@Override
		public String getType(Uri uri) {
			switch (uriMatcher.match(uri)) {
			case TUPLES :
				return BoardlListTable.CONTENT_TYPE; 
			case TUPLE :
				return BoardlListTable.CONTENT_ITEM_TYPE;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
		}

		@Override
		public Uri insert(Uri uri, ContentValues values) {
			if (uriMatcher.match(uri) != TUPLES){
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			SQLiteDatabase db = databaseHelper.getWritableDatabase();

			if(!values.containsKey(BoardlListTable.BOARD_NAME)){
				throw new IllegalArgumentException("Unknown URI " + uri);
			} 
			if(!values.containsKey(BoardlListTable.BOARD_URL)){
				throw new IllegalArgumentException("Unknown URI " + uri);
			} 
			//if(!values.containsKey(BoardlListTable.FAV)){} 

			long id = db.insert(BoardlListTable.TABLE_NAME, null, values);	
			db.close();
			uri = ContentUris.withAppendedId(uri, id);
			getContext().getContentResolver().notifyChange(uri, null);
			return uri;
		}

		@Override
		public boolean onCreate() {
			databaseHelper = new DatabaseHelper(getContext());
			return true;
		}

		@Override
		public Cursor query(Uri uri, String[] projection, String selection,
				String[] selectionArgs, String sortOrder) {
			SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
			builder.setTables(BoardlListTable.TABLE_NAME);
			switch (uriMatcher.match(uri)) {
				case TUPLES :
					builder.setProjectionMap(projectionMap);
					break;
				case TUPLE :
					builder.setProjectionMap(projectionMap);
					builder.appendWhere(BoardlListTable._ID+"=?");
					selectionArgs = appendSelectionArgs(selectionArgs,
                            new String[] { uri.getLastPathSegment() });
					break;
				default:
					throw new IllegalArgumentException("Unknown URI " + uri);
			}
			

            if (TextUtils.isEmpty(sortOrder)) {
                sortOrder = BoardlListTable.DEFAULT_SORT_ORDER;
            }

            SQLiteDatabase db = databaseHelper.getReadableDatabase();

            Cursor c = builder.query(db, projection, selection, selectionArgs,
                    null /* no group */, null /* no filter */, sortOrder);

            c.setNotificationUri(getContext().getContentResolver(), uri);
            return c;
		}

		@Override
		public int update(Uri uri, ContentValues values, String selection,
				String[] selectionArgs) {
			int count;
			SQLiteDatabase db; //= databaseHelper.getWritableDatabase();
			switch (uriMatcher.match(uri)) {
			case TUPLES :
				db = databaseHelper.getWritableDatabase();
				count = db.update(BoardlListTable.TABLE_NAME, values, selection, selectionArgs);
				break;
			case TUPLE :
				db = databaseHelper.getWritableDatabase();
				selection = concatenateWhere(
						BoardlListTable._ID + "="+ ContentUris.parseId(uri) ,selection);
				count = db.update(BoardlListTable.TABLE_NAME, values, selection, selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			db.close();
			getContext().getContentResolver().notifyChange(uri, null);
			return count;
		}
		 
	 }
///bad usage of Handler
	private final Handler uiHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			int w, h;
			w = msg.arg1;
			h = msg.arg2;
			switch (msg.what) {
				case MSG_RESIZE_LAYOUT:
					findViewById(R.id.frameLayout2).setLayoutParams(
							new LinearLayout.LayoutParams(w, h));
					break;
				case MSG_SEARCH:
					String srch =(String) msg.obj;
					if ("UPDATE".equalsIgnoreCase(srch)){
//                        pd = ProgressDialog.show(KomicaROMActivity.this, getString(R.string.updatinglist) , getString(R.string.pleasewait), true, false);
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                updateBBS();
//                                uiHandler.sendEmptyMessage(MSG_UPDATE_FINISH);
//                                pd.dismiss();
//                            }}).start();
                        new BBSUpdateTask().execute();
						break;
					}
                    if ("export".equalsIgnoreCase(srch)) {
                        doExport2GoogleDrive();
                        break;
                    }
                    if ("import".equalsIgnoreCase(srch)) {
                        doImport2GoogleDrive();
                        break;
                    }
                    if ("Komica2".equalsIgnoreCase(srch)){
//                        pd = ProgressDialog.show(KomicaROMActivity.this, getString(R.string.updatinglist), getString(R.string.pleasewait), true, false);
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                updateK2();
//                                uiHandler.sendEmptyMessage(MSG_UPDATE_FINISH);
//                                pd.dismiss();
//                            }}).start();
                        new K2UpdateTask().execute();
                        break;
					}
					List<String> list = new ArrayList<String>();
					Iterator<String> localIterator = KomicaROMActivity.this.boardList
							.iterator();
					while (localIterator.hasNext()) {
						String str = localIterator.next();
						String str1 = str.split(",")[1];
						if (str1 ==null) break;
						if (str1.contains(srch)) list.add(str);
					}
					if (!list.isEmpty()) KomicaROMActivity.this.srchBoardList = list;
					KomicaROMActivity.this.listView3
						.setAdapter(new KomicaROMActivity.SrchBoardAdapter(
								KomicaROMActivity.this.getApplication(),
								R.layout.row,
								KomicaROMActivity.this.srchBoardList));
					break;
				case MSG_LOAD_URL:
					//((ImageButton)findViewById(R.id.ImageButton1)).performClick();
					break;
//                case MSG_UPDATE_BBS:
////                    pd = ProgressDialog.show(KomicaROMActivity.this, getString(R.string.updatinglist), getString(R.string.pleasewait), true, false);
////                    new Thread(new Runnable() {
////                        @Override
////                        public void run() {
////                            updateBBS();
////                            uiHandler.sendEmptyMessage(MSG_UPDATE_FINISH);
////                            pd.dismiss();
////                        }}).start();
//                    new BBSUpdateTask().execute();
//                    //updateBBS();
//                    break;
//                case MSG_UPDATE_FINISH:
//                    initList();
//                    reloadListView();
//                    break;
//                case MSG_UPDATE_ERROR:
//                    Toast.makeText(KomicaROMActivity.this,"Update Fail",Toast.LENGTH_LONG).show();
//                    break;
				default:
					break;
			}
			super.handleMessage(msg);
		}

	};

	void initList() {
		String[] PROJECTION = new String[]{
				BoardlListTable._ID,
				BoardlListTable.BOARD_NAME,
				BoardlListTable.BOARD_URL,
				BoardlListTable.FAV
			};
		this.favBoardList = Collections.synchronizedList(new ArrayList<String>());
		this.boardList = new ArrayList<String>();
        boardList.add(strUPDATELIST+","+strUPDATELIST+",0"); //for update list
		StringBuilder buffer;
		int i =0;
		Cursor c = getContentResolver().query(BoardlListTable.CONTENT_URI, PROJECTION, null, null, null);
		if (c.moveToFirst()){
			buffer= new StringBuilder();
			while(true){
				i = c.getInt(c.getColumnIndex(BoardlListTable.FAV));
				buffer.append(c.getString(c.getColumnIndex(BoardlListTable.BOARD_URL)))
					.append(",")
					.append(c.getString(c.getColumnIndex(BoardlListTable.BOARD_NAME)))
					.append(",")
					.append(i);
				boardList.add(buffer.toString());
				if (i !=0){
					favBoardList.add(buffer.toString());
				}
				buffer.setLength(0);
				if (!c.moveToNext()){
					//end
					buffer=null;
					break;
				}
			}
		}
		c.close();
		
	}
	
	
	void initTab(SparseArray<Parcelable> container) {
		this.listView1 = ((ListView) findViewById(R.id.listView1));
		this.listView2 = ((ListView) findViewById(R.id.listView2));
		this.listView3 = ((ListView) findViewById(R.id.listView3));

		TabHost localTabHost = (TabHost) findViewById(R.id.tabhost);
		localTabHost.setup();
		TabHost.TabSpec localTabSpec1 = localTabHost.newTabSpec("Board");
		localTabSpec1.setContent(R.id.tab1);
		localTabSpec1.setIndicator("列表", getResources().getDrawable(android.R.drawable.ic_menu_sort_by_size));
		localTabHost.addTab(localTabSpec1);
		TabHost.TabSpec localTabSpec2 = localTabHost.newTabSpec("Fav");
		localTabSpec2.setContent(R.id.tab2);
		localTabSpec2.setIndicator("標記", getResources().getDrawable(android.R.drawable.btn_star_big_on));
		localTabHost.addTab(localTabSpec2);

		TabHost.TabSpec localTabSpec3 = localTabHost.newTabSpec("Srch");
		localTabSpec3.setContent(R.id.tab3);
		localTabSpec3.setIndicator("搜尋",
				getResources().getDrawable(android.R.drawable.ic_menu_search));
		localTabHost.addTab(localTabSpec3);
		KomicaROMActivity.this.srchBoardList = new ArrayList<String>();
		localTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String paramString) {
				Iterator<String> localIterator;

				localIterator = KomicaROMActivity.this.boardList.iterator();

				if (paramString.equals("Srch")) {
					return;
				}
				if (paramString.equals("Fav")) {
					KomicaROMActivity.this.favBoardList.clear();
				}
				while (true){
					if (localIterator.hasNext()) {
						String str = localIterator.next();
						if (str.endsWith(",1")){
							KomicaROMActivity.this.favBoardList.add(str);
						}
					} else {
						KomicaROMActivity.this.listView2
						.setAdapter(new KomicaROMActivity.FavBoardAdapter(
								KomicaROMActivity.this.getApplication(),
								R.layout.row,
								KomicaROMActivity.this.favBoardList));
						if (paramString.equals("Board"))
							((BaseAdapter) KomicaROMActivity.this.listView1
									.getAdapter()).notifyDataSetChanged();
						return;
					}
				}//end while
			}
		});
		localTabHost.getTabWidget().getChildAt(2).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!srchBoardList.isEmpty()) ((TabHost) findViewById(R.id.tabhost)).setCurrentTabByTag("Srch");
			 	final EditText input = new EditText(KomicaROMActivity.this);
                input.setSingleLine(true);
				new AlertDialog.Builder(KomicaROMActivity.this)
						.setTitle("搜尋")
						.setView(input)
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int whichButton) {
                                        // Do nothing.
                                    }
                                })
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialog,
											int whichButton) {
										String srch = input.getText()
												.toString();
										if (srch != null
												&& !srch.equals("")) {
											TabHost localTabHost = (TabHost) findViewById(R.id.tabhost);
											localTabHost.setCurrentTabByTag("Srch");
											uiHandler.obtainMessage(MSG_SEARCH, srch).sendToTarget();
										}
									}
								})
                        .show();
			}
		});
		if (container!=null) localTabHost.restoreHierarchyState(container);
		localTabHost=null;
	}

	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);

        // Initialize Firebase Remote Config.
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
// Define Firebase Remote Config Settings.
        FirebaseRemoteConfigSettings firebaseRemoteConfigSettings =
                new FirebaseRemoteConfigSettings.Builder()
                        .setDeveloperModeEnabled(false)
                        .build();
// Define default config values. Defaults are used when fetched config values are not
// available. Eg: if an error occurred fetching values from the server.
        HashMap<String, Object> defaultConfigMap = new HashMap<>();
        defaultConfigMap.put("liveurl", "http://2cat.or.tl/~tedc21thc/live");
        defaultConfigMap.put("k1url",  "http://web.komica.org/bbsmenu.html");
        defaultConfigMap.put("k2url",  "http://pink.komica.org/bbsmenu.htm");

// Apply config settings and default values.
        mFirebaseRemoteConfig.setConfigSettings(firebaseRemoteConfigSettings);
        mFirebaseRemoteConfig.setDefaults(defaultConfigMap);
// Fetch remote config.
        fetchConfig();

		//houseKeepDatabase();
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
		setContentView(R.layout.main);
		initList();
		if( paramBundle ==null){
			initTab(null);
		}else{
			initTab(paramBundle.getSparseParcelableArray("TABHOST"));
		}
		reloadListView();
		
		enableHttpResponseCache();
		this.webView = ((WebView) findViewById(R.id.webView1));
		WebSettings ws = this.webView.getSettings();
		ws.setJavaScriptEnabled(true);
		ws.setBuiltInZoomControls(true);
		ws.setPluginState(PluginState.ON_DEMAND);
		CookieSyncManager.createInstance(webView.getContext());
		CookieManager cm = CookieManager.getInstance();
		cm.setCookie("http://youtube.com", "PREF=\"f1=50000000&f2=40000000\"; Domain=.youtube.com");
		CookieSyncManager.getInstance().sync();
		ws.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
		ws.setDisplayZoomControls(false);//API11
		ws.setSupportZoom(true);
		ws.setRenderPriority(RenderPriority.NORMAL);
		this.webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		this.webView.setWebChromeClient(webChromeClient);
//		this.webView.setLongClickable(true);
		this.webView.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				HitTestResult result = webView.getHitTestResult();
				int type= result.getType();
				switch (type) {
				case HitTestResult.IMAGE_TYPE:
//					new DownloadTask().execute(result.getExtra());

					String url = result.getExtra();
					Uri u = Uri.parse(url);
					DownloadManager.Request request = new DownloadManager.Request(u);
					request.setDescription("Image");
					request.setTitle(u.getLastPathSegment());
					request.allowScanningByMediaScanner();
					request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
					request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, u.getLastPathSegment());

// get download service and enqueue file
					DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
					manager.enqueue(request);

					Log.d("IMAGE_TYPE",result.getExtra());
					break;
				case HitTestResult.SRC_IMAGE_ANCHOR_TYPE:
					Intent i;
					i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(result.getExtra()));
					startActivity(i);
					Log.d("SRC_IMAGE_ANCHOR_TYPE",result.getExtra());
					break;
				case HitTestResult.SRC_ANCHOR_TYPE:
					Intent ii;
					ii = new Intent(Intent.ACTION_VIEW);
					ii.setData(Uri.parse(result.getExtra()));
					startActivity(ii);
					Log.d("SRC_ANCHOR_TYPE",result.getExtra());
					break;
				default:
					break;
				}

//
//				if (type == HitTestResult.IMAGE_TYPE || type == HitTestResult.SRC_IMAGE_ANCHOR_TYPE){
//					Log.e("Looooong",result.getExtra());
//
//					//new DownloadTask().execute(result.getExtra());
//				}
//
				return true;
			}
		});
//		this.webView.setOnTouchListener(new OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				return false;
//			}
//		});
		this.webView.setWebViewClient(new WebViewClient(){
			/* (non-Javadoc)
			 * @see android.webkit.WebViewClient#onPageFinished(android.webkit.WebView, java.lang.String)
			 */
			@Override
			public void onPageFinished(WebView view, String url) {
				KomicaROMActivity.this.setProgressBarVisibility(false);
				final String u = url.toLowerCase();
				if (u.startsWith(LIVE_URL) && experimentalFeature){
					//view.loadUrl("javascript:console.log('MAGIC'+document.getElementsByTagName('html')[0].innerHTML);");
					if(ngList==null || ngList.size()==0){
						readNGLIST();
					}
					if (ngList!=null && ngList.size()>0){
						for (String ngid : ngList){
							//StringBuffer sb= new StringBuffer();
							//String banSpan = "<td bgcolor=\"#AF0FC7\">ID:"+ngid+" is Banned</td>";
					    	//sb.append("ID:").append(ngid).append(" No[.].+?</td>");
					    	// var y=document.getElementsByTagName('BODY')[0].innerHTML.replace(/ID:Wer.+\n.+\n.+<\/td>/gm,'999999</tr><br/>'); document.getElementsByTagName('BODY')[0].innerHTML =y;
					    	//ID:3n6hJh5k No
					    	//document.getElementsByTagName("BODY")[0].innerHTML 
					    	String jscript = "javascript:";
							//String ngid="3n6hJh5k";
							//jscript += "var y=document.getElementsByTagName('BODY')[0].innerHTML.replace(/ID:"+ngid+"(.+\\n)?.+<\\/td>/gm,'ID:"+ngid+" is blocked by program</td><blockquote></blockquote><br/>');"+
							//		"document.getElementsByTagName('BODY')[0].innerHTML =y;";
					    	//jscript += "document.getElementsByTagName('BODY')[0].innerHTML =document.getElementsByTagName('BODY')[0].innerHTML.replace(/ID:("+ngid+".+)(\\n.+)?<\\/td>/gm,'ID:$1 Blocked by program<blockquote><br/></blockquote></td>');";
					    	//jscript +=" for( var v in document.getElementsByTagName('TABLE')){if (v.innerHTML.indexOf('ID:"+ngid+"')>-1) v.style.display='none';};";
					    	//jscript +=" for( var v =0;v < document.getElementsByTagName('table').length;v++) document.getElementsByTagName('table')[v].style.display='none';";
					    	jscript +=" for( var v =0;v < document.getElementsByTagName('td').length;v++) if (document.getElementsByTagName('td')[v].innerHTML.indexOf('ID:"+ngid+"')>-1) document.getElementsByTagName('td')[v].innerHTML='ID:"+ngid+" is blocked';";
					    	view.loadUrl(jscript);
					    	//Log.d("JSCR",jscript);
							//Log.d("NGLIST", ngid);
							//Log.d("URL",view.getUrl());
						}
					}
				}				
				//view.loadUrl("javascript:console.log('MAGIC1'+document.getElementsByTagName('html')[0].innerHTML);");
				view.getSettings().setLoadsImagesAutomatically(true);
				super.onPageFinished(view, url);
			}

			/* (non-Javadoc)
			 * @see android.webkit.WebViewClient#onPageStarted(android.webkit.WebView, java.lang.String, android.graphics.Bitmap)
			 */
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				KomicaROMActivity.this.setProgressBarVisibility(true);
				KomicaROMActivity.this.setProgressBarIndeterminate(true);
				if (url.toLowerCase().startsWith(LIVE_URL)){
					view.getSettings().setLoadsImagesAutomatically(false);
				}else{
					view.getSettings().setLoadsImagesAutomatically(true);
				}
				super.onPageStarted(view, url, favicon);
			}

			/* (non-Javadoc)
			 * @see android.webkit.WebViewClient#shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String)
			 */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				final String u = url.toLowerCase();
				String s =  MimeTypeMap.getFileExtensionFromUrl(url);
				s = MimeTypeMap.getSingleton().getMimeTypeFromExtension (s);
				s=(s==null)?"":s;
//Log.d("MIME",  s);
				if (s.startsWith("application")){
					Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
					startActivity(i);
					return true;
				}
				if (s.startsWith("image")){
					String parms[] ={url,view.getUrl()};
					bulletText("背景下載中⋯⋯");
					//new DownloadTask().execute( parms );
					Uri uri = Uri.parse(parms[0]);
                    DownloadManager.Request request = new DownloadManager.Request(uri);
                    //request.setDescription("Image");
                    request.setTitle(uri.getLastPathSegment());
					request.allowScanningByMediaScanner();
					request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
					request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());

// get download service and enqueue file
                    DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);
					return true;
				}
				if (u.startsWith(WebView.SCHEME_MAILTO)){
					//int duration = Toast.LENGTH_SHORT;
					//if (u.length() >20 ) duration = Toast.LENGTH_LONG;
					try {
						//Toast.makeText(getApplicationContext(), URLDecoder.decode(url.substring(7), "UTF-8"), duration).show();
						bulletText(URLDecoder.decode(url.substring(7), "UTF-8"));
					} catch (UnsupportedEncodingException e) {
						//e.printStackTrace();
					}
					return true;
				}
				
				return super.shouldOverrideUrlLoading(view, url);
			}
				
			});
		if (paramBundle !=null){
			webView.restoreState(paramBundle);
			webView.scrollTo(0, paramBundle.getInt("scroll", 0));
		}else{
            webView.loadUrl("file:///android_asset/komica.html");
		}
 

		mDeceleration = //getResources().getDisplayMetrics().density
				 (386.0878F * getResources().getDisplayMetrics().ydpi)
				* ViewConfiguration.getScrollFriction();

		findViewById(R.id.checkedTextViewUp).setOnClickListener(
				new View.OnClickListener() {
					public void onClick(View paramView) {
						KomicaROMActivity.this.webView.flingScroll(
								0,
								-(int) Math
										.sqrt(2.0D * (0.8D * KomicaROMActivity.this.webView
												.getHeight() * KomicaROMActivity.mDeceleration)));
					}
				});
		findViewById(R.id.checkedTextViewDown).setOnClickListener(
				new View.OnClickListener() {
					public void onClick(View paramView) {
						KomicaROMActivity.this.webView.flingScroll(
								0,
								(int) Math
										.sqrt(2.0D * (0.8D * KomicaROMActivity.this.webView
												.getHeight() * KomicaROMActivity.mDeceleration)));
				
					}
				});
		findViewById(R.id.ImageButton1).setOnClickListener(
				new View.OnClickListener() {
					public void onClick(View paramView) {
						ImageButton button = (ImageButton) paramView;
						View v = findViewById(R.id.RelativeLayout2);
						if ("hide".equals(button.getTag())) {
							if (lockCount==0)v.setTag(v.getWidth());
							lockCount++;
							button.setTag("show");
							button.setImageDrawable(getResources().getDrawable(
									android.R.drawable.ic_media_next));
							v.setVisibility(View.GONE);
						} else {
							lockCount++;
							button.setTag("hide");
							button.setImageDrawable(getResources().getDrawable(
									android.R.drawable.ic_media_previous));

							v.setVisibility(View.VISIBLE );
						}
						//TEST
						//bulletText("Large Text"+SystemClock.currentThreadTimeMillis());
					}
				});
		
//		findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				TextView tv = (TextView) findViewById(R.id.autoCompleteTextView1);
//				String ss = tv.getText().toString();
//				if (ss.length() != 8){
//
//					return;
//				}
//				final WifiManager wm = (WifiManager) getBaseContext().getSystemService(Context.WIFI_SERVICE);
//				if (wm ==null) return;
//				final String tmWifiMac = wm.getConnectionInfo().getMacAddress();
////Log.d("ID", tmWifiMac);
//
////			 	List results = new ArrayList<Map>();
////				results.add(new HashMap<String,String>("entry.0.single", ss));
////				results.add(new HashMap<String,String>("entry.1.single", tmWifiMac.replace(":", "")));
//				String results = "data: {\"entry.0.single\" : " + "" +"\"ss\"" +", \"entry.1.single\" : \""+tmWifiMac.replace(":", "") +"\"}";
//
//				submitGoogleForm("dFdvaEYyaWxPTkl5Nl9valozdDdsVnc6MQ",results);
//
//				if (ngList ==null){
//					readNGLIST();
//				}
//				ngList.add(ss);
//
//			}
//		});
		
		
		
	}

	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
		//if (webView == null) webView = (WebView) findViewById(R.id.webView1);
		if ((paramInt == KeyEvent.KEYCODE_BACK) && (this.webView.canGoBack())) {
			this.webView.goBack();
			return true;
		}
		if (paramInt==KeyEvent.KEYCODE_MENU && experimentalFeature){
			View v = findViewById(R.id.relativeLayout1);
			if (v.getVisibility() == View.VISIBLE){
				v.setVisibility(View.INVISIBLE);
			}else if (this.webView.getUrl().startsWith(LIVE_URL)){
				v.setVisibility(View.VISIBLE);
			}
		}
		
		return super.onKeyDown(paramInt, paramKeyEvent);
	}

	private class BoardAdapter extends ArrayAdapter<String> {
		final List<String> objects;
		final int resourceId;

		public BoardAdapter(Context c, int i, List<String> localList) {
			super(c, i, localList);
			this.objects = localList;
			this.resourceId = i;
		}

		public View getView(int paramInt, View paramView,
				ViewGroup paramViewGroup) {
			if (paramView == null)
				paramView = ((LayoutInflater) getContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE)).inflate(this.resourceId, null);
			String[] arrayOfString =  this.objects.get(paramInt).split(",");
			TextView localTextView = (TextView) paramView
					.findViewById(R.id.CheckedTextView1);
			localTextView.setText(arrayOfString[1]);
			localTextView.setTag(arrayOfString[0]);
			CompoundButton localCompoundButton = (CompoundButton) paramView
					.findViewById(R.id.CheckBox1);
			localCompoundButton.setTag(paramInt);
			localCompoundButton.setChecked(arrayOfString[2].equals("1"));
			localCompoundButton
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						public void onCheckedChanged(
								CompoundButton paramCompoundButton,
								boolean paramBoolean) {
							int i = (Integer) paramCompoundButton.getTag();
							String str1 = BoardAdapter.this.objects
									.remove(i);
							
							if (paramBoolean) {
								str1 = str1.replace(",0", ",1");
							} else {
								str1 = str1.replace(",1", ",0");
							}
							String[] ss =str1.split(",");
							ContentValues values = new ContentValues();
							values.put(BoardlListTable.FAV, paramBoolean);
							getContentResolver().update(BoardlListTable.CONTENT_URI, values, "BOARDURL = ? AND BOARDNAME = ?", new String[]{ss[0],ss[1]});
							values.clear();
							values=null;
							KomicaROMActivity.BoardAdapter.this.objects.add(i,
									str1);
							KomicaROMActivity.this.boardList = BoardAdapter.this.objects;
						}
					});
			localTextView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View paramView) {
                    String url =(String) paramView.getTag();
                    if (url.equalsIgnoreCase("更新列表")){
                     //   updateBBS();
                     //   uiHandler.sendEmptyMessage(MSG_UPDATE_BBS);
                        new BBSUpdateTask().execute();
                    }else{
                        KomicaROMActivity.this.webView.loadUrl(url);
                        uiHandler.sendEmptyMessage(MSG_LOAD_URL);
                    }
				}
			});
			return paramView;
		}
	}

	private class FavBoardAdapter extends ArrayAdapter<String> {
		final List<String> fav;
		final int resourceId;

		public FavBoardAdapter(Context c, int i, List<String> localList) {
			super(c, i, localList);
			this.fav = localList;
			this.resourceId = i;
		}

		public View getView(int paramInt, View paramView,
				ViewGroup paramViewGroup) {
			if (paramView == null)
				paramView = ((LayoutInflater) getContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE)).inflate(this.resourceId, null);
			String[] arrayOfString = this.fav.get(paramInt)
					.split(",");
			TextView localTextView = (TextView) paramView
					.findViewById(R.id.CheckedTextView1);
			localTextView.setText(arrayOfString[1]);
			localTextView.setTag(arrayOfString[0]);
			localTextView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View paramView) {
					KomicaROMActivity.this.webView.loadUrl((String) paramView
							.getTag());
					uiHandler.sendEmptyMessage(MSG_LOAD_URL);
				}
			});
			CompoundButton localCompoundButton = (CompoundButton) paramView
					.findViewById(R.id.CheckBox1);
			localCompoundButton.setTag(paramInt);
			localCompoundButton.setChecked(true);
			localCompoundButton
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						public void onCheckedChanged(
								CompoundButton paramCompoundButton,
								boolean paramBoolean) {
							int i = (Integer) paramCompoundButton.getTag();

//							((View) paramCompoundButton.getParent())
//									.setVisibility(View.GONE);
						
								String str = favBoardList.remove(i);
//								Log.e("onchecked",str);
								listView2.setAdapter( new FavBoardAdapter(getApplicationContext(), R.layout.row, favBoardList));
								listView2. setSelection(i);
								String ss[] = str.split(",");
								ContentValues values = new ContentValues();
								values.put(BoardlListTable.FAV, 0);
								getContentResolver().update(BoardlListTable.CONTENT_URI, values, "BOARDURL = ? AND BOARDNAME = ?", new String[]{ss[0],ss[1]});

								values.clear();
								values=null;
								//
								int j = 0;
								Iterator<String> localIterator = KomicaROMActivity.this.boardList
										.iterator();
								while (true) {
									if (!localIterator.hasNext()){
										return  ;
									}	
									if (str.equals(localIterator.next())) {
										KomicaROMActivity.this.boardList.remove(j);
										KomicaROMActivity.this.boardList.add(j,
												str.replace(",1", ",0"));
										return  ;
									}
									j++;
							 
								}
						}
					});
			return paramView;
		}
	}

	private class SrchBoardAdapter extends ArrayAdapter<String> {
		final List<String> objects;
		final int resourceId;

		public SrchBoardAdapter(Context c, int i, List<String> localList) {
			super(c, i, localList);
			this.objects = localList;
			this.resourceId = i;
		}

		public View getView(int paramInt, View paramView,
				ViewGroup paramViewGroup) {
			if (paramView == null)
				paramView = ((LayoutInflater) getContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE)).inflate(this.resourceId, null);
			String[] arrayOfString = this.objects.get(paramInt)
					.split(",");
			TextView localTextView = (TextView) paramView
					.findViewById(R.id.CheckedTextView1);
			localTextView.setText(arrayOfString[1]);
			localTextView.setTag(arrayOfString[0]);
			CompoundButton localCompoundButton = (CompoundButton) paramView
					.findViewById(R.id.CheckBox1);
			localCompoundButton.setTag(paramInt);
			localCompoundButton.setChecked(arrayOfString[2].equals("1"));
			localCompoundButton
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						public void onCheckedChanged(
								CompoundButton paramCompoundButton,
								boolean paramBoolean) {
							int i = (Integer) paramCompoundButton.getTag();
							String str = SrchBoardAdapter.this.objects
									.remove(i);
							String str1="";
							int j = 0;
							Iterator<String> localIterator = KomicaROMActivity.this.boardList
									.iterator();
							if (paramBoolean) {
								str1 = str.replace(",0", ",1");
							} else {
								str1 = str.replace(",1", ",0");
							}
							//
							String ss[] = str.split(",");
							ContentValues values = new ContentValues();
							values.put(BoardlListTable.FAV, paramBoolean);
							getContentResolver().update(BoardlListTable.CONTENT_URI, values, "BOARDURL = ? AND BOARDNAME = ?", new String[]{ss[0],ss[1]});
							values.clear();
							values=null;
							//
							KomicaROMActivity.SrchBoardAdapter.this.objects.add(i,str1);
							srchBoardList = SrchBoardAdapter.this.objects;
							while (true) {
								if (!localIterator.hasNext())
									return;
								if (str.equals(localIterator.next())) {
									KomicaROMActivity.this.boardList.remove(j);
									KomicaROMActivity.this.boardList.add(j,
											str1);
									return;
								}
								j++;
							}
						}
					});
			localTextView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View paramView) {
					KomicaROMActivity.this.webView.loadUrl((String) paramView
							.getTag());
					uiHandler.sendEmptyMessage(MSG_LOAD_URL);
				}
			});
			return paramView;
		}
	}

	
	private void reloadListView(){
		this.listView1.setAdapter(new BoardAdapter(getApplication(),
				R.layout.row, this.boardList));
		this.listView2.setAdapter(new FavBoardAdapter(getApplication(),
				R.layout.row, this.favBoardList));
		this.listView3.setAdapter(new SrchBoardAdapter(getApplication(),
				R.layout.row, this.srchBoardList));
		TabHost tab = (TabHost) findViewById(R.id.tabhost);
		
		String tag = tab.getCurrentTabTag();
		if ("Board".equals(tag) && !favBoardList.isEmpty()){
			tab.setCurrentTabByTag("Fav");
		}
	} 

	/* (non-Javadoc)
	 * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (webView !=null){
			webView.saveState(outState);
			outState.putInt("scroll", webView.getScrollY());
		}
		SparseArray<Parcelable> container =  new SparseArray<Parcelable>();
		( findViewById(R.id.tabhost)).saveHierarchyState(container);
		outState.putSparseParcelableArray("TABHOST", container);
		super.onSaveInstanceState(outState);
	}
	
//	private void submitGoogleForm(String formid, String listValuePair){
//		final String url ="https://spreadsheets.google.com/spreadsheet/formResponse?formkey=";
//		//formid ="dFdvaEYyaWxPTkl5Nl9valozdDdsVnc6MQ";
////		 HttpClient client = new DefaultHttpClient();
////		 HttpPost post = new HttpPost(url+formid);
////		 if( (listValuePair!=null) && (!listValuePair.isEmpty())){
////		    try {
////				post.setEntity(new UrlEncodedFormEntity(listValuePair));
////				Toast.makeText(getApplicationContext(), "Submited,Thanks", Toast.LENGTH_SHORT).show();
////			} catch (UnsupportedEncodingException e) {
////				e.printStackTrace();
////			}
////		    try {
////				client.execute(post);
////			} catch (ClientProtocolException e) {
////				e.printStackTrace();
////			} catch (IOException e) {
////				e.printStackTrace();
////			}
////		 }
//		HttpURLConnection urlConnection =null;
//		try {
//			urlConnection = (HttpURLConnection) new URL(url).openConnection();
//			urlConnection.setDoOutput(true);
//			urlConnection.setChunkedStreamingMode(0);
//			OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
//			out.write(listValuePair.getBytes());
//			out.flush();
//		}catch(IOException e){
//		} finally {
//			if (urlConnection !=null ) urlConnection.disconnect();
//		}
//	}
	  
	  private final WebChromeClient webChromeClient = new WebChromeClient(){
		  @SuppressWarnings("unused")
		  public void openFileChooser(ValueCallback<Uri> uploadMsg) {  
				  mUploadMessage = uploadMsg;  
				  Intent i = new Intent(Intent.ACTION_GET_CONTENT);  
				  i.addCategory(Intent.CATEGORY_OPENABLE);  
				  i.setType("image/*");  
				  KomicaROMActivity.this.startActivityForResult(Intent.createChooser(i,getString(R.string.chooseImage)), FILECHOOSER_RESULTCODE);  
             }

             @SuppressWarnings("unused")
			public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                 openFileChooser(uploadMsg);
             }                   

             @SuppressWarnings("unused")
			public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                 openFileChooser(uploadMsg);
             }               
		  
	  };

	  @Override
	  protected void onActivityResult(int requestCode, int resultCode,
	          Intent intent) {
          Log.d("resultCode" ,resultCode+"");
          Log.d("requestCode" ,requestCode+"");
          switch (requestCode){
              case FILECHOOSER_RESULTCODE:
                  if (null == mUploadMessage)
                      return;
                  Uri result = intent == null || resultCode != RESULT_OK ? null
                          : intent.getData();
                  mUploadMessage.onReceiveValue(result);
                  mUploadMessage = null;
                  break;
              case REQUEST_EXPORT_AGAIN:
                  if (resultCode == RESULT_OK){
                      if (mEmail.equals("")) {
                          mEmail = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                      }
                      doExport2GoogleDrive();
                  }else{
                      mEmail="";
                  }
                  break;
              case REQUEST_IMPORT_AGAIN:
                  if (resultCode == RESULT_OK){
                      if (mEmail.equals("")) {
                          mEmail = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                      }
                      doImport2GoogleDrive();
                  }else{
                      mEmail="";
                  }
                  break;
              default:
                  break;
          }

	  }
	  
	  void bulletText(final CharSequence cs){
			TextView tv = (TextView) findViewById(R.id.bulletTextView);
			tv.setVisibility(View.VISIBLE);
			tv.setText( cs );
			tv.startAnimation( AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bullet));
			tv.setVisibility(View.INVISIBLE);
	  }

    class BBSUpdateTask extends AsyncTask<Void,Void,Void>{
        boolean done = false;
        int i;
        String message;

         @Override
         protected Void doInBackground(Void[] params) {
             done=false;
             LinkedHashMap <String,String> localMap =new LinkedHashMap <String,String>();
             try {
                 for (Element link: Jsoup.connect(K1_URL).get().select("a[href]")){
                     if (link.attr("target").length()==0){
                         localMap.put(link.text(),link.attr("abs:href"));
                         publishProgress();
                     }
                 }

             } catch (MalformedURLException e) {
                 Log.e(KomicaROMActivity.this.getLocalClassName(),"MalformedURLException" );
                 localMap.clear();
             } catch (IOException e) {
                 localMap.clear();
                 Log.e(KomicaROMActivity.this.getLocalClassName(),"IOException");
             }
             if ( localMap.isEmpty()){
                 //update fail
//                 localMap=null;
                 FirebaseCrash.report(new Exception("BBS Update Fail : "+ K1_URL));
                 return null;
             }

             ContentResolver cr = getContentResolver();
             cr.delete(BoardlListTable.CONTENT_URI, " 1=1 ", null);
             ContentValues cv = new ContentValues();
             for (String key : localMap.keySet() ){
                 cv.put(BoardlListTable.BOARD_NAME, key);
                 cv.put(BoardlListTable.BOARD_URL, localMap.get(key));
                 cr.insert(BoardlListTable.CONTENT_URI, cv);
                 cv.clear();
             }
             for (String s : favBoardList){
                 String[] ss = s.split(",");
                 cv.put(BoardlListTable.FAV, true);
                 cr.update(BoardlListTable.CONTENT_URI, cv, BoardlListTable.BOARD_NAME+"=?", new String[]{ss[1]});
                 cv.clear();
             }
             done = true;
//             cv=null;
//             cr=null;
             return null;
         }

         @Override
         protected void onPreExecute() {
             super.onPreExecute();
             pd = ProgressDialog.show(KomicaROMActivity.this, getString(R.string.updatinglist) , getString(R.string.pleasewait), true, false);
             i=0;
         }

         @Override
         protected void onPostExecute(Void o) {
             super.onPostExecute(o);
             if (!done){
                 message ="Update Fail";
             }else{
                 message ="Update Okay";
             }
             initList();
             reloadListView();
             if (pd!=null )pd.dismiss();
             pd=null;
             done=false;
             Toast.makeText(KomicaROMActivity.this,message,Toast.LENGTH_LONG).show();
             i=0;
         }

         @Override
         protected void onProgressUpdate(Void[] values) {
             super.onProgressUpdate(values);
             i++;
             pd.setMessage("已讀入 "+i+" 項");
         }

         @Override
         protected void onCancelled(Void o) {
             done=false;
             if (pd!=null )pd.dismiss();
             pd=null;
             Toast.makeText(KomicaROMActivity.this,"Update Cancel",Toast.LENGTH_LONG).show();
             super.onCancelled(o);
         }

         @Override
         protected void onCancelled() {
             if (pd!=null )pd.dismiss();
             pd=null;
             Toast.makeText(KomicaROMActivity.this,"Update Cancel",Toast.LENGTH_LONG).show();
             super.onCancelled();
         }
     }

    class K2UpdateTask extends AsyncTask<Void,Void,Void>{
        boolean done = false;
        String message;
        int i;
        @Override
        protected Void doInBackground(Void[] params) {
            done=false;
            LinkedHashMap <String,String> localMap =new LinkedHashMap <String,String>();
            i=0;
            try {
                for (Element link:Jsoup.connect(K2_URL).get().select("a[href]")){
                    if (link.attr("target").length()==0){
                        localMap.put("K2-"+link.text(),link.attr("abs:href"));
                        i++;
                        publishProgress();
                    }
                }

            } catch (MalformedURLException e) {
                //e.printStackTrace();
                localMap.clear();
            } catch (IOException e) {
                localMap.clear();
            }
            i=0;
            if ( localMap.isEmpty()){
                //update fail
				FirebaseCrash .report( new Exception( "K2 Update Fail : "+ K2_URL));
				return null;
            }

            ContentResolver cr = getContentResolver();

            ContentValues cv = new ContentValues();
            for (String key : localMap.keySet() ){
                cv.put(BoardlListTable.BOARD_NAME, key);
                cv.put(BoardlListTable.BOARD_URL, localMap.get(key));
                cr.insert(BoardlListTable.CONTENT_URI, cv);
                cv.clear();
            }
            done=true;
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(KomicaROMActivity.this, getString(R.string.updatinglist) , getString(R.string.pleasewait), true, false);
        }

        @Override
        protected void onPostExecute(Void o) {
            super.onPostExecute(o);
            if (!done){
                message ="Update Fail";
            }else{
                message ="Update Okay";
            }
            initList();
            reloadListView();
            if (pd!=null )pd.dismiss();
            pd=null;
            done=false;
            Toast.makeText(KomicaROMActivity.this,message,Toast.LENGTH_LONG).show();

        }

        @Override
        protected void onProgressUpdate(Void[] values) {
            super.onProgressUpdate(values);
            pd.setMessage("已讀入 "+i+" 項");
        }

        @Override
        protected void onCancelled(Void o) {
            super.onCancelled(o);
            done=false;
            if (pd!=null )pd.dismiss();
            pd=null;
            Toast.makeText(KomicaROMActivity.this,"Update Cancel",Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (pd!=null )pd.dismiss();
            pd=null;
            Toast.makeText(KomicaROMActivity.this,"Update Cancel",Toast.LENGTH_LONG).show();
        }
    }

    class ExportTask extends AsyncTask
            implements GoogleApiClient.OnConnectionFailedListener{

        final Activity activity;
        GoogleApiClient googleApiClient;
        int i=0;

        ExportTask(Activity activity ) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(KomicaROMActivity.this, "匯出至Google Drive", getString(R.string.pleasewait), true, false);
            i=0;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (googleApiClient!=null){
                googleApiClient.disconnect();
            }
            if (pd!=null){
                pd.dismiss();
                pd=null;
            }
            i=0;
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            super.onProgressUpdate(values);
            i++;
            pd.setMessage("已匯出 "+i+" 項");
        }

        void pickUserAccount() {
            String[] accountTypes = new String[]{"com.google"};
            Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                    accountTypes, false, null, null, null, null);
            activity.startActivityForResult(intent, REQUEST_EXPORT_AGAIN);

        }
        /**
         * Executes the asynchronous job. This runs when you call execute()
         * on the AsyncTask instance.
         */
        @Override
        protected Object doInBackground(Object[] params) {
            String token;
            try {
                Log.d("BG","run");
                if (mEmail.equals("")) {
                    pickUserAccount();
                    return null;
                }
                Log.d("Email",mEmail);
                token = fetchToken();
                if (token == null ) {
                    return null;
                }
                Log.d("Token   ",token);

                 googleApiClient = new GoogleApiClient.Builder(activity)
                        .addApi(Drive.API)
                        .addScope(Drive.SCOPE_FILE)
                        .addOnConnectionFailedListener(this)
                        .setAccountName(mEmail)
                        .build();
                googleApiClient.blockingConnect();

                DriveApi.DriveContentsResult driveContentsResult =
                        Drive.DriveApi.newDriveContents(googleApiClient).await();
                if (!driveContentsResult.getStatus().isSuccess()) {
                    Log.d("MAYDAY", "We failed, stop the task and return.");
                    return null;
                }

                // Read the contents and open its output stream for writing, then
                // write a short message.
                DriveContents originalContents = driveContentsResult.getDriveContents();
                OutputStream os = originalContents.getOutputStream();
                try {
                    Iterator<String> localIterator = KomicaROMActivity.this.boardList
                            .iterator();
                    localIterator.next();
                    while (localIterator.hasNext()) {
                        String str = localIterator.next();
                        os.write((str+"\n").getBytes());
                        publishProgress();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
                // Create the metadata for the new file including title and MIME
                // type.
                MetadataChangeSet originalMetadata = new MetadataChangeSet.Builder()
                        .setTitle("KomicaSimpleBrowserSetting.txt")
                        .setStarred(true)
                        .setMimeType("text/plain").build();

                // Create the file in the root folder, again calling await() to
                // block until the request finishes.
                DriveFolder rootFolder = Drive.DriveApi.getRootFolder(googleApiClient);
                DriveFolder.DriveFileResult fileResult = rootFolder.createFile(
                        googleApiClient, originalMetadata, originalContents).await();
                if (!fileResult.getStatus().isSuccess()) {
                    Log.d("MAYDAY", "  We failed, stop the task and return.");
                    return null;
                }
                Log.d("fileResult",fileResult.toString());
            } catch (IOException e) {
                // The fetchToken() method handles Google-specific exceptions,
                // so this indicates something went wrong at a higher level.
                // TIP: Check for network connectivity before starting the AsyncTask.
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Gets an authentication token from Google and handles any
         * GoogleAuthException that may occur.
         */
        String fetchToken() throws IOException {
            try {
               Log.d("Email",mEmail);
                return GoogleAuthUtil.getToken(activity, mEmail, "oauth2:https://www.googleapis.com/auth/drive.file");
            } catch (UserRecoverableAuthException userRecoverableException) {
                activity.startActivityForResult(
                        userRecoverableException.getIntent(),
                        REQUEST_EXPORT_AGAIN);
                Log.d("Export Again","2");
            } catch (GoogleAuthException fatalException) {
                fatalException.printStackTrace();
            }
            return null;
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.d("onConnectionFailed",connectionResult.toString());
            if (!connectionResult.hasResolution()) {
                GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), activity, 0).show();
                return;
            }
            try {
                connectionResult.startResolutionForResult(activity, REQUEST_EXPORT_AGAIN);
                Log.d("Export Again","1");
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }

        }
    }

    class ImportTask extends AsyncTask
            implements GoogleApiClient.OnConnectionFailedListener{

        final Activity activity;
        GoogleApiClient googleApiClient;
        int i=0;
        boolean ex=false;
        @Override
        protected void onProgressUpdate(Object[] values) {
            super.onProgressUpdate(values);
            i++;
            pd.setMessage("已匯入 "+i+" 項");
        }

        ImportTask(Activity activity ) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(activity,"匯入中","請稍後");
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            initList();
            reloadListView();
            pd.dismiss();
            pd=null;
            if (ex) Toast.makeText( activity, "Import Fail",Toast.LENGTH_LONG).show();
        }

        void pickUserAccount() {
            String[] accountTypes = new String[]{"com.google"};
            Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                    accountTypes, false, null, null, null, null);
            activity.startActivityForResult(intent, REQUEST_IMPORT_AGAIN);

        }
        /**
         * Executes the asynchronous job. This runs when you call execute()
         * on the AsyncTask instance.
         */
        @Override
        protected Object doInBackground(Object[] params) {
            String token;
            try {
                Log.d("BG", "run");
                if (mEmail.equals("")) {
                    pickUserAccount();
                    return null;
                }
                Log.d("Email", mEmail);
                token = fetchToken();
                if (token == null) {
                    return null;
                }
                Log.d("Token   ", token);

                googleApiClient = new GoogleApiClient.Builder(activity)
                        .addApi(Drive.API)
                        .addScope(Drive.SCOPE_FILE)
                        .addOnConnectionFailedListener(this)
                        .setAccountName(mEmail)
                        .build();
                googleApiClient.blockingConnect();

                SortOrder sortOrder = new SortOrder.Builder()
                        .addSortDescending(SortableField.MODIFIED_DATE).build();

                Query query = new Query.Builder()
                        .addFilter(Filters.eq(SearchableField.TITLE, "KomicaSimpleBrowserSetting.txt"))
                        .setSortOrder(sortOrder)
                        .build();
                Log.d("StepA", "A");

                MetadataBuffer metadataBuffer = Drive.DriveApi.query(googleApiClient, query).await().getMetadataBuffer();
                DriveId driveId = metadataBuffer.get(0).getDriveId();
                DriveContents contents = Drive.DriveApi.getFile(googleApiClient, driveId).open(googleApiClient, DriveFile.MODE_READ_ONLY, null).await().getDriveContents();
                Log.d("contents", contents.toString());
                BufferedReader reader = new BufferedReader(new InputStreamReader(contents.getInputStream()));

                String line;
                ContentResolver cr = getContentResolver();
                cr.delete(BoardlListTable.CONTENT_URI, " 1=1 ", null);
                while ((line = reader.readLine()) != null) {
                    if (line.length() > 0) {
                        String[] s = line.split(",");
                        ContentValues cv = new ContentValues();
                        cv.put(BoardlListTable.BOARD_URL, s[0]);
                        cv.put(BoardlListTable.BOARD_NAME, s[1]);
                        cv.put(BoardlListTable.FAV, s[2].equals("1"));
                        cr.insert(BoardlListTable.CONTENT_URI, cv);
                        cv.clear();

                    }
                    publishProgress();
                }
                contents.discard(googleApiClient);
            }catch (Exception e){
                //fail import
                ex=true;
            }
            return null;
        }

        /**
         * Gets an authentication token from Google and handles any
         * GoogleAuthException that may occur.
         */
        String fetchToken() throws IOException {
            try {
                Log.d("Email",mEmail);
                return GoogleAuthUtil.getToken(activity, mEmail, "oauth2:https://www.googleapis.com/auth/drive.file");
            } catch (UserRecoverableAuthException userRecoverableException) {
                activity.startActivityForResult(
                        userRecoverableException.getIntent(),
                        REQUEST_IMPORT_AGAIN);
            } catch (GoogleAuthException fatalException) {
                fatalException.printStackTrace();
            }
            return null;
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            if (!connectionResult.hasResolution()) {
                GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), activity, 0).show();
                return;
            }
            try {
                connectionResult.startResolutionForResult(activity, REQUEST_IMPORT_AGAIN);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }

        }
    }

    void doExport2GoogleDrive(){
        new ExportTask(this).execute();
    }

    void doImport2GoogleDrive(){
        new ImportTask(this).execute();
    }

}
